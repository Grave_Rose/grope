# Gr@ve_Rose's Offensive PCap Enumerator (GROPE)

### Overview
GROPE is a tool to assist with PenTesting and Red Team events where stealth is required. By running a packet capture on a planted (or previously pwn3d device) you can then enumerate what devices were seen on the network without having to send any recon traffic yourself. 

### What it is and isn't

  - **Will:** Ingest a previously run PCap file
  - **Won't:** Run a PCap for you
  - **Will:** Output to text and CSV with a "|" as the delimiter
  - **Won't:** Output to HTML or other format (yet)
  - **Will:** Take raw data from noisy traffic and attempt to enumerate devices
  - **Won't:** Run nMap for you
  - **Will:** Provide you a list with Ethernet, IPv4, IPv6, hostname and some OS information
  - **Won't:** Tell you how to pwn the targets
  
### Pre-Requisites
GROPE relies on having most built-in tools (sed, grep, awk) as well as having tshark installed. A check will be performed every time the program is run and will alert you if there are missing dependencies. I would suggest running this **before** having your engagement start to ensure that all dependencies are met.

### How it Works
Why read when you check out a short video: https://www.youtube.com/watch?v=X1_e-tXE5As

A lot of network devices are "noisy" in the sense that they send out a lot of traffic which is not unicast. For example, Windows machines will send out NetBIOS information. All IPv4 hosts **must** send out ARP requests to communicate. Switching devices may use LLDP or CDP to talk with one another. VRRP, OSPF and other protocols will send out multicast traffic. When you run a packet capture, anything like the above being sent out on the same Layer-2 domain can be seen by anyone else on the same Layer-2 domain. If you plug a device into an access port on, let's say, VLAN 10, you can see all broadcast and multicast traffic (depending on switch security configuration) without having to send anything yourself.

Even better is if you plug into a switch port which is configured as a trunk port with the "native vlan" set to a user VLAN. Now your device can have an IP address and see broadcast and multicast traffic on the other VLANs assigned to the trunk. This happens more often than you'd like to see. ;)

GROPE will read the packet capture and uniquely sort the MAC addresses. It will then run through a set of enumerators and extract information from the packets with _tshark_ and group the output based on MAC address together. For example, if your PCap has an ARP request from 1.1.1.1 with a MAC of 00:11:22:33:44:55 and a NetBIOS broadcast from the same MAC, these two pieces of information will be beside on another in the report files.

### Example Output and Analysis
I've had to scrub this however this is a valid single entry from the text output of a real-world Red Team engagement...


```
Device Info
MAC Address: 00:11:22:33:44:55
VLAN ID: 
IPv4 Address: 1.1.1.1
IPv4 Destinations: 1.1.1.255
IPv6 Address: fe80::c0ff:ee01:bad:c0de
IPv6 Destinations: ff02::1:2
Hostname Info: redacted.example.com redacted
Operating System Info: MSFT 5.0 Windows Server 2008R2 or Windows 7
Modules Matched: dhcpv6 nbnsb
```

What does this tell us, exactly? Let's break it down by line...
> MAC Address: 00:11:22:33:44:55

Here we know the Layer-2 address of a device. Maybe we can use this for session hijacking.

> VLAN ID: 

If there is no VLAN ID seen, this device is on the same network as us or the target is using a hub instead of a switch. Seeing as it's not 1997 anymore, likely on the same network as us. :) If the VLAN ID **does** show up, we're likely plugged into a trunk port.

> IPv4 Address: 1.1.1.1

Now we have a target to attack when the time comes.

> IPv4 Destinations: 1.1.1.255

If we weren't on the same network as the target (if we were plugged into a trunk port) we now know the subnet size of the 1.1.1.x network (/24) since we have the broadcast address of the packet.

> IPv6 Address: fe80::c0ff:ee01:bad:c0de

Not a lot of companies log link-local traffic so feel free to _nmap -6 fe80::c0ff:ee01:bad:c0de%eth0_ when you're ready.

> IPv6 Destinations: ff02::1:2

We know that the device is looking for a DHCPv6 server. Could potentially set up a DHCPv6 server and scoop some traffic.

> Hostname Info: redacted.example.com redacted

Thanks to NetBIOS we have a domain name and a hostname. The domain could come in handy when it's time to attack Active Directory.

> Operating System Info: MSFT 5.0 Windows Server 2008R2 or Windows 7

Based on the NetBIOS information pulled, we can assume that this is either a Windows Server 2008R2 box or a Win7 box. This should help identify any _Metasploit_ modules you want to load.

> Modules Matched: dhcpv6 nbnsb

This just lets us know which modules matched for the information which was extracted from the PCap.

### How to use
The first thing you need is a packet capture. I suggest the following (assuming _eth0_ is your adapter name):

` tcpdump -nn -vvv -e -s 0 -X -c 1000 -w file.pcap -i eth0`

_(Personal plug: I've also created https://tcpdump101.com to help people build PCaps on different platforms. Check it out if you want to learn more about PCaps.)_

This command will capture one thousand packets on eth0 and store them in the file called "file.pcap". Once you have that file, run GROPE against it:

`$: grope.sh -i ./file.pcap -o my_output`

Wait for the spinners to stop spinning and you'll end up with two files:

`my_output` which is the CSV pipe-seperated file
`my_output.txt` which is the flat-file and easy to read with `less`

### Contrib and Contact
   - You can reach me on Twitter at https://twitter.com/Grave_Rose or e-mail me at tcpdump101 -at- gmail [dot] com
   - Feel free to create suggestions however pull requests will be denied for now. I plan to open this up later on so please don't spam me on when this will happen.  
   - Monetary donations can be made here: https://www.paypal.me/tcpdump101 or with BTC at https://www.blockchain.com/btc/address/1AYBCHhoHc4SfgVuwAXZuhF1B4FWXajynG or LTC at https://live.blockcypher.com/ltc/address/LMGPbbUeHxewcBkNTCxSoRqmrF5LNcGfYT/